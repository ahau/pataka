# Pataka Changelog

## v4.3.0

Features
- stop multiple instances opening in windows
- bundle backend for faster startup

Patches
- update `ssb-ahoy` (`electron`, `secret-stack`)
- update `electron-builder`

## v4.2.0

New Features:
- web registration form for tribes


## v4.1.0

???


## v4.0.0

???


## v3.0.0

???


## v2.0.1

New Features
- added version tag in the bottom corner

## v2.0.0

Extracted this from the ahau (whakapapa-ora repo)

New Features
- added auto-updater


